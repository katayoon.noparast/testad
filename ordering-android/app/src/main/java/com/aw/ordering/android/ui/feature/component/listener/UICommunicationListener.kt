package com.aw.ordering.android.ui.feature.component.listener

/**
 * Purpose - Provide common methods used in app.
 * @author Created by Ravindra
 * Created - 08-08-2022
 */

interface UICommunicationListener {

    //for displaying progress bar
    fun displayProgressBar(isLoading :Boolean)

    //hiding keyboard
    fun hideKeyboard()

    //checking permission granted like storage, location
    fun isStoragePermissionGranted() :Boolean


}