package com.aw.ordering.android.ui.feature.base

import androidx.appcompat.app.AppCompatActivity
import com.aw.ordering.android.ui.feature.component.listener.UICommunicationListener

/**
 * Created by Ravindra
 */

abstract class BaseActivity: AppCompatActivity() , UICommunicationListener {

    /**
     * include all the base methods like progress bar , permission , keyboard
     */
}