package com.aw.ordering.android

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

/**
 * Purpose -Base Application class of app.
 * @author Created by Ravindra
 * Created - 08-08-2022
 */

@HiltAndroidApp
class BaseApplication() :Application(){
    override fun onCreate() {
        super.onCreate()

        //if build is in debug mode only then print log statements
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }
}